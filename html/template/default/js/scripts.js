(function ($) {
	$(function(){

		navCollapse();
		accordion();
		slider();

	});
})(jQuery);

$(window).on('load resize',function(){

	if( $(window).width() <= 640 ) {
        
		$('.ul_product_feature').bxSlider({
			auto: true,
			speed: 1000,
			controls: false,
			pager: false
		});
	}

});

function slider() {

	if( $('.ul_product_slider').not('.disabled').length) {
		var slider = $('.ul_product_slider').not('.disabled').bxSlider({
			auto: true,
			speed: 1000,
			controls: false,
			pager: true,
			pagerCustom: '#bx_pager_product'
		});

		$(window).on('load resize',function(e){
			e.preventDefault();
			if( $(window).width() <= 640 ) {
				slider.reloadSlider({
				    auto: true,
					speed: 1000,
					controls: false,
					pager: true,
					pagerCustom: null
				});
			}
			else {
				slider.reloadSlider({
				    auto: true,
					speed: 1000,
					controls: false,
					pager: true,
					pagerCustom: '#bx_pager_product'
				});
			}
		});
	}

	if( $('.ul_top_slider').length ) {

		$('.ul_top_slider').bxSlider({
			auto: true,
			speed: 1000,
			controls: true,
			pager: true
		});
	}
}

function backTop() {
	/*
	$(window).scroll(function() {
		if($(window).scrollTop() != 0) {
				$('#backtop').fadeIn();
		} else {
				$('#backtop').fadeOut();
		}
	});
	*/
	$('#backtop').click(function() {
		$('html, body').animate({scrollTop:0},500);
	});
}

function anchorAnimate() {
	$('.anchor a').click(function() {
		var speed = 500;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$('body,html').animate({scrollTop:position}, speed, 'swing');
		return false;
	 });
}

function navCollapse() {
	$("#btn_menu").click(function() {
		$(".nav_menu_sm_inner").stop().fadeIn('300');
		$("body").addClass('ovh');
	});
	$("#btn_close").click(function() {
		$(".nav_menu_sm_inner").stop().fadeOut('300');
		$("body").removeClass('ovh');
	});
}

function accordion() {

	$('.accordion > li > a').each(function(){
		var element = $(this);
		var checkE = element.next();
		if(checkE.is('ul')) {
			$(this).closest('li').addClass('has');
		}
	});
	$('.accordion > li > a').click(function() {

		var checkElement = $(this).next();

		$('.accordion li').removeClass('active');

		if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
			$(this).closest('li').removeClass('active');
			checkElement.slideUp('normal');
		}

		if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
			$(this).closest('li').addClass('active');
			$('.accordion ul:visible').slideUp('normal');
			checkElement.slideDown('normal');
		}

		if (checkElement.is('ul')) {
			return false;
		} else {
			return true;
		}
	});
}

function tgle() {
	$('.toogle .toogle_ttl').click(function() {
		var element = $(this);
		var checkElement = element.next();
		checkElement.stop().slideToggle('300');
	});
}

function tabs() {
	$('.tabs').each(function(){
		var $active, $content, $links = $(this).find('a');
		$active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
		$active.addClass('active');
		$content = $($active.attr('href'));
		$links.not($active).each(function () {
			$($(this).attr('href')).hide();
		});
		$(this).on('click', 'a', function(e){
		$active.removeClass('active');
		$content.hide();
		$active = $(this);
		$content = $($(this).attr('href'));
		$active.addClass('active');
		$content.show();
		e.preventDefault();
		});
	});
}

