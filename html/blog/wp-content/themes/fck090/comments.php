<div class="fblike_and_comment">
	<?php wp_social_bookmarking_light_output_e(); ?>
<!--	<div class="fb-like" data-href="#" data-layout="button_count" data-action="like" data-show-faces="true"></div>-->
	<a class="trans comment" href="#">コメント <?php comments_number('(0)','(1)','(%)'); ?></a>
</div>
<p class="date_title">コメント <?php comments_number('はまだありません。','(1)','(%)'); ?> <a href="#contact_form" class="gb_link">»</a></p>
<p class="mt10">
<!--	コメントはまだありません。<br>-->
<ol class="commentlist clearfix">
	<?php
	$args = array(
		'walker'            => null,
		'max_depth'         => '',
		'callback'          => null,
		'end-callback'      => null,
		'type'              => 'all',
		'page'              => '',
		'per_page'          => '',
		'reverse_top_level' => null,
		'reverse_children'  => '',
		'reply_text' => '',
	);
	wp_list_comments($args); ?>
</ol>
	<br>トラックバック URL<br>
	<div class="group_textinput_plus">
		<input type="text" value="<?php trackback_url(); ?>" class="text_input"  style="width: 100%;" readonly>
	</div>
</p>
<p class="date_title mt30">コメントをどうぞ</p>
<div style="display: none;"><?php comment_form(); ?></div>
<form action="./" method="get" id="contact_form">
	<div class="md">
		<div class="tb_contact_form tb fms">
			<div class="tr">
				<div class="th">お名前<span class="required">【必須】</span></div>
				<div class="td">
					<div class="group_textinput_plus">
						<input type="text" class="text_input" name="name">
					</div>
				</div>
			</div>
			<div class="tr">
				<div class="th">メール (公開されません)<span class="required">【必須】</span></div>
				<div class="td">
					<div class="group_textinput_plus">
						<input type="text" class="text_input" name="email">
					</div>
				</div>
			</div>
			<div class="tr">
				<div class="th">ウェブサイト</div>
				<div class="td">
					<div class="group_textinput_plus">
						<input type="text" class="text_input" name="url">
					</div>
				</div>
			</div>
			<div class="tr tr_space vtop">
				<div class="th"></div>
				<div class="td">
					<textarea class="text_area" name="comment"></textarea>
				</div>
			</div>
		</div>
	</div>
	<div class="sm">
		<div class="tb_contact_form_sm fms">
			<dl>
				<dt>お名前<span class="required">【必須】</span></dt>
				<dd>
					<div class="group_textinput_plus">
						<input type="text" class="text_input" name="name">
					</div>
				</dd>
			</dl>
			<dl>
				<dt>メール (公開されません)<span class="required">【必須】</span></dt>
				<dd>
					<div class="group_textinput_plus">
						<input type="text" class="text_input" name="email">
					</div>
				</dd>
			</dl>
			<dl>
				<dt>ウェブサイト</dt>
				<dd>
					<div class="group_textinput_plus">
						<input type="text" class="text_input" name="url">
					</div>
				</dd>
			</dl>
			<dl>
				<dt></dt>
				<dd>
					<textarea class="text_area" name="comment"></textarea>
				</dd>
			</dl>
		</div>
	</div>
	<ul class="form_btn md">
		<li>
			<a href="#" class="btn_reset trans">
				<img src="<?php echo get_template_directory_uri(); ?>/img/contact/btn_reset.png" alt="リセット">
			</a>
		</li><li>
			<a href="#" class="btn_confirm trans">
				<img src="<?php echo get_template_directory_uri(); ?>/img/contact/btn_send.png" alt="送信">
			</a>
		</li>
	</ul>
</form>
<div class="mod_contact_btn">
	<ul class="ul_contact_btn sm">
		<li>
			<a href="#" class="btn_back trans">
				<img src="<?php echo get_template_directory_uri(); ?>/img/contact/sm/btn_reset.png" alt="リセット" />
			</a>
		</li><li>
			<a href="#" class="btn_send trans">
				<img src="<?php echo get_template_directory_uri(); ?>/img/contact/sm/btn_confirm.png" alt="送信内容の確認" />
			</a>
		</li>
	</ul>
</div>
<style>
	.commentlist .comment {
		margin-top: 18px;
		width: 100%;
	}
	.comment-body .comment-author {
		float: left;
	}
	.comment-body .comment-meta {
		float: right;
	}
	.comment-body > p {
		clear: both;
		margin-left: 10px;
	}
</style>
<script>
	$(function() {
		$('a.btn_confirm').click(function() {
			var name = $('div.md [name="name"]').val();
			var email = $('div.md [name="email"]').val();
			var url = $('div.md [name="url"]').val();
			var comment = $('div.md [name="comment"]').val();

			$('#author').val(name);
			$('#email').val(email);
			$('#url').val(url);
			$('#comment').val(comment);
			$('#submit').click();
		});

		$('a.btn_send').click(function() {
			var name = $('div.sm [name="name"]').val();
			var email = $('div.sm [name="email"]').val();
			var url = $('div.sm [name="url"]').val();
			var comment = $('div.sm [name="comment"]').val();

			$('#author').val(name);
			$('#email').val(email);
			$('#url').val(url);
			$('#comment').val(comment);
			$('#submit').click();
		});

	});
</script>
