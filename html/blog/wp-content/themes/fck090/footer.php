</main><!-- /.CONTENT  -->
<!-- FOOTER  -->
<footer>
	<div class="footer_inner md">
		<div class="row clearfix">
			<div class="col">
				<div class="mod_footer_info block_pay">
					<h3 class="mod_ttl"><span>お支払い方法について</span></h3>
					<div class="mod_cont">
						<div class="thumb"><img src="<?php echo get_template_directory_uri(); ?>/img/common/footer_pay.png" alt="支払い方法" /></div>
						<div class="cont fms">
							クレジットカード、代金引換、<br />
							銀行振り込みをご利用いただけます。
						</div>
					</div>
				</div>
				<div class="mod_footer_info block_ship">
					<h3 class="mod_ttl"><span>送料について</span></h3>
					<div class="mod_cont">
						<div class="ship">
							<dl>
								<dt>
									送料全国一律1,000円<small>(北海道、沖縄、一部離島は1,500円)</small>
								</dt>
								<dd>
									1配送先につき、<br />
									10,800円以上のご購入で<br />
									全国送料無料!!
								</dd>
							</dl>
						</div>
						<div class="cont fms">
							※離島・一部地域は追加料金がかかる場合があります。<br />
							※商品サイズの大きな商品につきましては、別途料金がかかる場合がございます。<br /><br />

							●送料込み商品の扱い<br />
							送料は送料料金表に関係なく送料無料になります。ただし、商品ページで個別に送料が設定されている商品がある場合は、その送料×個数分は必要になります。<br /><br />

							●まとめ買い時の扱い<br />
							送料は上記料金表の1個分送料になります。
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="mod_footer_info block_delivery">
					<h3 class="mod_ttl"><span>配送について</span></h3>
					<div class="mod_cont">
						<div class="cont fms">
							<p>
								ヤマト運輸、佐川急便、ゆうパック<br />
								運送業者は、お住まいの地域や状況により変わります。配送業者のご指定は頂けませんので、ご了承くださいませ。<br /><br />

								【商品発送のタイミング】<br />
								お届けは注文された商品が揃い次第、まとめて発送いたします。最短のお届けをご希望の方は指定しないで下さい。<br /><br />

								配送日時の指定は下記からお選びいただけます。
							</p>
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/img/common/footer_time.png" alt="配送時間帯" />
							</figure>
							<p>
								※ただし時間を指定された場合でも、事情により指定時間内に配達が出来ない事がございます。<br /><br />

								●特にご指定がない場合<br />
								楽天バンク決済、銀行振込、ゆうちょ振替　⇒ご入金確認後、3営業日以内に発送いたします。<br />
								クレジット、代金引換　⇒ご注文確認後、3営業日以内に発送いたします。
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="mod_footer_access">
					<a href="/help/guide" class="btn trans"><img src="<?php echo get_template_directory_uri(); ?>/img/common/btn_first.png" alt="初めての方はこちら" /></a>
				</div>
				<div class="mod_footer_info block_exchanged">
					<h3 class="mod_ttl"><span>返品・交換について</span></h3>
					<div class="mod_cont">
						<div class="cont fms">
							商品の品質・管理については万全を期しておりますが、万一商品の不良・誤送・不足があった場合は、お届け後7日間以内にご連絡下さい。<br />
							メールにて詳しいご返送方法のご案内をご返信致しますので、そちらを必ずご確認頂きご返送お願い致します。<br />
							お客様ご都合(商品の破損・汚損以外)による返品・交換は承っておりません。予めご了承下さい。<br /><br />

							＜注文確定後のキャンセル・変更について＞<br />
							当日発送をしておりますので、注文確定後のキャンセル・変更は致しかねます。誠に恐れ入りますが、商品到着後に対応をさせて頂きます。
						</div>
					</div>
				</div>
				<div class="mod_footer_contact">
					<div class="thumb">
						<img src="<?php echo get_template_directory_uri(); ?>/img/common/logo_w.png" alt="四万十うなぎ" />
					</div>
					<div class="cont fms">
						四万十うなぎ株式会社<br />
						【本社】高知県高岡郡四万十町見付896-6<br />
						TEL.0880-22-1468FAX.0880-22-2502
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer_inner_sm sm">
		<ul class="ul_footer_info">
			<li class="info_contact">
				<div class="ttl"><img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/contact_ttl.png" alt="お問い合わせ先" /></div>
				<div class="desc">営業時間／午前10時～午後5時(※土日祝日は休み)</div>
			</li>
			<li class="info_tel">
				<img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/contact_tel.png" alt="088-803-7108" />
			</li>
			<li class="info_mail">
				<div class="desc"><span>info@shimanto-unagi.com</span></div>
			</li>
			<li class="info_note">
				<div class="desc">●お気軽にお問い合わせ下さい。</div>
			</li>
		</ul>
	</div>
	<div class="footer_info">
		<ul class="footer_nav">
			<li>
				<a href="/" class="trans">HOME</a>
			</li><li>
				<a href="/hepl/about" class="trans">会社情報</a>
			</li><li>
				<a href="/help/tradelaw" class="trans">特定商取引について</a>
			</li><li>
				<a href="/help/privacy" class="trans">個人情報保護方針</a>
			</li><li>
				<a href="/contact" class="trans">お問い合わせ</a>
			</li>
		</ul>
		<div class="copyright fms">
			Copyright (C ) 2015 MAR U雑貨 All Rights Reserved.<br />
			禁無断複製、無断転載、 このホームページに掲載されている<br class="sm" />記事・写真・図表などの 無断転載を禁じます。
		</div>
	</div>
</footer><!-- /.FOOTER  -->
</div>
</div><!-- /.container -->

</body>
</html>