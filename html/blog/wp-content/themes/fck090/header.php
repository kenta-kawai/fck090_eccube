<!DOCTYPE html>
<!--[if lt IE 9]><html class="ie8" lang="ja"><![endif]-->
<!--[if (gt IE 9)|!(IE)]>--> <html lang="ja"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="HandheldFriendly" content="True" />
	<meta name="MobileOptimized" content="320" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="高知県の四万十川の水で厳しい管理の下、優しく育てたうなぎ(鰻)の蒲焼は身が柔らかく安心・安全・おいしい自慢の国産うなぎです。" />
	<meta name="keywork" content="うなぎ,蒲焼き,白焼き,ちまき,鰻,ウナギ,四万十,高知,土佐,通販" />
	<title><?php wp_title(); ?></title>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/mobile.css" />
	<!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="cleartype" content="on" />
	<![endif]-->
	<!--[if lt IE 9]>
	<script src="/js/html5shiv.min.js"></script>
	<script src="/js/IE9.js"></script>
	<script src="/js/respond.min.js"></script>
	<![endif]-->
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.2.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>
</head>
<body>
<div id="container">
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.5";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	<div class="main_content wrapper">
		<!-- HEADER for pc  -->
		<header class="md">
			<div class="header_inner clearfix">
				<div class="logo"><a href="../"><img src="<?php echo get_template_directory_uri(); ?>/img/common/logo.png" alt="四万十うなぎ" /></a></div>
				<div class="top_bar">
					<div class="login_info">
						<a href="/cart" class="btn_login trans"><img src="<?php echo get_template_directory_uri(); ?>/img/common/btn_cart.png" alt="お買い物カゴ" /></a>
						<a href="/mypage" class="btn_login trans"><img src="<?php echo get_template_directory_uri(); ?>/img/common/btn_mypage.png" alt="マイページ" /></a>
					</div>
				</div>
				<div class="bottom_bar">
					<div class="mod_pay"><img src="<?php echo get_template_directory_uri(); ?>/img/common/header_pay.png" alt="支払い方法" /></div>
					<div class="mod_access">
						<div class="contact_tel"><img src="<?php echo get_template_directory_uri(); ?>/img/common/header_contact.png" alt="オンラインショップお問い合わせ窓口 088-803-7108 お気軽にお問い合わせください。お問い合わせフォームはこちら" /></div>
						<a href="/contact" class="contact_btn trans">
							<img src="<?php echo get_template_directory_uri(); ?>/img/common/ico_mail.png" alt="contact" />
						</a>
					</div>
				</div>
			</div>
			<nav>
				<ul class="navbar clearfix">
					<li><a href="/help/guide"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav01.png" alt="お買い物ガイド" /></a></li>
					<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav02.png" alt="お支払い・配送について" /></a></li>
					<li><a href="/user_data/faq"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav03.png" alt="よくあるご質問" /></a></li>
					<li><a href="/help/about"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav04.png" alt="会社情報" /></a></li>
					<li><a href="/user_data/appeal"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav05.png" alt="四万十うなぎのこだわり" /></a></li>
				</ul>
			</nav>
		</header><!-- /.HEADER for pc  -->

		<!-- HEADER for sp -->
		<div class="header_sm sm">
			<div class="header_top">四万十川で取れたシラスを厳しい管理の下、優しく育てた自慢のうなぎ</div>
			<div class="header_inner">
				<div class="logo">
					<a href="../"><img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/logo.png" alt="四万十うなぎ" /></a>
				</div>
				<div class="top_bar clearfix">
					<a href="/cart" class="btn_cart"><img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/btn_cart.png" alt="買い物カゴ" /></a>
					<div id="btn_menu"><img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/btn_menu.png" alt="menu" /></div>
				</div>
				<div class="bottom_bar">
					<div class="login_info">
						<a href="/mypage" class="btn_login"><img src="<?php echo get_template_directory_uri(); ?>/img/common/btn_mypage.png" alt="マイページ" /></a>
					</div>
					<div class="register_info">
						<img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/header_pay.png" alt="支払い方法">
					</div>
				</div>
			</div>
		</div><!-- HEADER for sp -->
		<!-- NAV for sp -->
		<div class="nav_sm sm">
			<ul class="navbar clearfix">
				<li class="nav01">
					<a href="/help/guide"><img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/nav01.png" alt="お買い物ガイド" /></a>
				</li>
				<li class="nav02">
					<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/nav02.png" alt="お支払い・配送について" /></a>
				</li>
				<li class="nav03">
					<a href="/help/about"><img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/nav03.png" alt="会社情報" /></a>
				</li>
			</ul>
			<ul class="navbar one_half clearfix">
				<li class="nav04">
					<a href="/user_data/faq"><img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/nav04.png" alt="よくあるご質問" /></a>
				</li>
				<li class="nav05">
					<a href="/user_data/appeal"><img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/nav05.png" alt="四万十うなぎのこだわり" /></a>
				</li>
			</ul>
		</div><!-- NAV for sp -->

		<!-- Menu for sp -->

		<div class="nav_menu_sm sm">
			<div class="nav_menu_sm_inner">
				<div class="nav_head clearfix">
					<h3 class="ttl"><img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/menu_open_ttl.png" alt="メニュー" /></h3>
					<div class="btn clearfix">
						<a href="/cart" class="btn_cart_open"><img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/btn_cart_open.png" alt="買い物カゴ" /></a>
						<div id="btn_close"><img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/btn_close.png" alt="close" /></div>
					</div>
				</div>
				<div class="nav_cont">
					<ul class="navbar_open accordion">
						<li>
							<a href="/"><span>トップページ</span></a>
						</li>
						<li>
							<a href="/help/guide"><span>初めての方へ</span></a>
						</li>
						<li>
							<a href="/user_data/appeal"><span>四万十うなぎのこだわり</span></a>
						</li>
						<li>
							<a href="#"><span>商品カテゴリから選ぶ</span></a>
							<ul>
								<li><a href="/products/list"><span>全商品を見る</span></a></li>
								<li><a href="/products/list?category_id=7"><span>蒲焼き</span></a></li>
								<li><a href="/products/list?category_id=8"><span>塩うなぎ</span></a></li>
								<li><a href="/products/list?category_id=9"><span>白焼き</span></a></li>
								<li><a href="/products/list?category_id=10"><span>ちまき</span></a></li>
								<li><a href="/products/list?category_id=11"><span>珍味</span></a></li>
								<li><a href="/products/list?category_id=12"><span>セット商品</span></a></li>
								<li><a href="/products/list?category_id=13"><span>まとめ買い</span></a></li>
							</ul>
						</li>
						<li>
							<a href="/user_data/faq"><span>よくあるご質問</span></a>
						</li>
						<li>
							<a href="/contact"><span>お問い合わせ</span></a>
						</li>
						<li>
							<a href="<?php bloginfo("url") ?>"><span>四万十うなぎからのお知らせ</span></a>
						</li>
						<li>
							<a href="/help/about"><span>会社情報</span></a>
						</li>
						<li>
							<a href="/help/tradelaw"><span>特定商取引について</span></a>
						</li>
						<li>
							<a href="/help/privacy"><span>個人情報保護方針</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<main class="page_about clearfix">