<?php get_header(); ?>
<?php get_sidebar(); ?>
<?php if(have_posts()): while(have_posts()): the_post(); $cats = get_the_category(); ?>
<div class="main_post">

	<div class="block_pathway">
		<ul class="ul_pathway md">
			<li><a href="/">TOP</a></li><li><a href="<?php bloginfo("url") ?>">お知らせ</a></li><li><span><?php the_title(); ?></span></li>
		</ul>
		<ul class="ul_pathway sm">
			<li><a href="/">TOP</a></li><li><a href="<?php bloginfo("url") ?>">お知らせ</a></li><li><span><?php the_title(); ?></span></li>
		</ul>
	</div><!-- /.block_pathway -->

	<div class="mod_shopping block_about gb_block block_news">
		<div class="news_excerpt">
			<p class="date_title"><?php the_time('Y年m月d日'); ?></p>
			<a class="trans art_title gb_link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a><!-- --><br><!--
                            --><small>カテゴリー:<?php foreach($cats as $cat): ?><a href="#"><?php echo $cat->name; ?></a><?php endforeach; ?>
				—  <?php the_author(); ?> @ <?php echo (get_the_time('A') == 'AM') ? '午前': '午後';  ?> <?php the_time('g:i'); ?></small>
			<br><br>
			<p class="news_content"><?php the_content(); ?></p>
			<?php comments_template(); ?>
		</div>
	</div>
</div><!-- / .main_post -->
<?php endwhile; endif; ?>
<?php get_footer(); ?>