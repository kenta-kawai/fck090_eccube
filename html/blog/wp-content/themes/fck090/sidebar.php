<!-- sidebar -->
<div class="sidebar md">

	<div class="mod_category">
		<div class="mod_ttl">
			<img src="<?php echo get_template_directory_uri(); ?>/img/common/category_ttl.png" alt="商品カテゴリ" class="md" />
			<img src="<?php echo get_template_directory_uri(); ?>/img/common/sm/category_ttl.png" alt="商品カテゴリから選ぶ" class="sm" />
		</div>
		<div class="mod_cont">
			<ul class="ul_category">
				<li><a href="/products/list"><span>全商品を見る</span></a></li>
				<li><a href="/products/list?category_id=7"><span>蒲焼き</span></a></li>
				<li><a href="/products/list?category_id=8"><span>塩うなぎ</span></a></li>
				<li><a href="/products/list?category_id=9"><span>白焼き</span></a></li>
				<li><a href="/products/list?category_id=10"><span>ちまき</span></a></li>
				<li><a href="/products/list?category_id=11"><span>珍味</span></a></li>
				<li class="sm"><a href="/products/list?category_id=12"><span>セット商品</span></a></li>
				<li class="sm"><a href="/products/list?category_id=13"><span>まとめ買い</span></a></li>
			</ul>
		</div>
	</div><!--/.mod_category -->

	<!--<div class="mod_calendar">
		<div class="mod_ttl"><img src="<?php echo get_template_directory_uri(); ?>/img/common/calendar_ttl.png" alt="営業日カレンダー" /></div>
		<div class="mod_cont">
			<div class="tb_calendar tb">
				<div class="tr">
					<div class="th">日</div>
					<div class="th">月</div>
					<div class="th">火</div>
					<div class="th">水</div>
					<div class="th">木</div>
					<div class="th">金</div>
					<div class="th">土</div>
				</div>
				<div class="tr">
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
				</div>
				<div class="tr">
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
				</div>
				<div class="tr">
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
				</div>
				<div class="tr">
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
				</div>
				<div class="tr">
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
					<div class="td">&nbsp;</div>
				</div>
			</div>
			<div class="tb_caption"><span>定休日</span></div>
		</div>
	</div><!--/.mod_calendar -->

	<div class="mod_hours">
		<div class="mod_ttl"><img src="<?php echo get_template_directory_uri(); ?>/img/common/hours_ttl.png" alt="営業時間 午前10時〜午後5時" /></div>
		<div class="mod_cont">
			当店へのお問い合わせは上記の時間帯で受け付けております。なお、インターネットでのご注文は24時間受け付けております。
		</div>
	</div><!-- /.mod_hours -->

	<div class="mod_contact">
		<div class="mod_ttl"><img src="<?php echo get_template_directory_uri(); ?>/img/common/contact_ttl.png" alt="お問い合わせ先" /></div>
		<div class="mod_cont">
			<div class="tel">TEL. 088-803-7108</div>
			<div class="cont">
				<a href="/contact/" class="trans">
					ホームページから<br />問い合わせする
				</a>
			</div>
		</div>
	</div><!-- /.mod_contact -->

	<div class="mod_adv">
		<ul class="ul_adv">
			<li>
				<div class="thumb">
					<a href="../#" class="trans"><img src="<?php echo get_template_directory_uri(); ?>/img/common/adv01.jpg" alt="直営店 うなぎ料理「うなきち」" /></a>
				</div>
				<div class="cont">
					四万十うなぎ(株)直営店<br />うなぎ料理「うなきち」
				</div>
			</li>
			<li>
				<div class="thumb">
					<a href="../#" class="trans"><img src="<?php echo get_template_directory_uri(); ?>/img/common/adv02.jpg" alt="ユースホステル" /></a>
				</div>
				<div class="cont">
					うなぎ料理「うなきち」<br />の3階・4階は<br />ユースホステル
				</div>
			</li>
		</ul>
	</div><!-- /.mod_adv -->

	<div class="mod_twitter">
		<img src="<?php echo get_template_directory_uri(); ?>/img/common/mod_twitter.png" alt="twitter" />
	</div><!--/.mod_twitter -->

</div><!-- ./sidebar -->