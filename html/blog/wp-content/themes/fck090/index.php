<?php get_header(); ?>
<?php get_sidebar(); ?>
<div class="main_post">

	<div class="block_pathway">
		<ul class="ul_pathway">
			<li><a href="../">TOP</a></li><li><span>お知らせ一覧</span></li>
		</ul>
	</div><!-- /.block_pathway -->

	<div class="block_hot_news">
		<div class="block_head clearfix">
			<h2 class="ttl">
				<img src="<?php echo get_template_directory_uri(); ?>/img/top/news_ttl.png" alt="四万十うなぎからのお知らせ" class="md">
				<img src="<?php echo get_template_directory_uri(); ?>/img/top/sm/news_ttl.png" alt="四万十うなぎからのお知らせ" class="sm">
			</h2>
		</div>
		<div class="block_cont">
			<div class="list_hot_news">
				<?php if(have_posts()): while(have_posts()): the_post(); ?>
					<dl class="tb">
						<dt class="td"><?php the_time('Y/m/d'); ?></dt>
						<dd class="td"><a href="<?php the_permalink(); ?>" class="trans"><?php the_title(); ?></a></dd>
					</dl>
				<?php endwhile; endif; ?>
			</div>
		</div>
		<div class="block_footer sm">
			<a href="news/" class="btn_more trans"><span>お知らせ一覧を見る</span></a>
		</div>
	</div>

	<div class="mod_shopping block_about gb_block block_news">
		<?php if(have_posts()): while(have_posts()): the_post(); $cats = get_the_category(); ?>
			<div class="news_excerpt">
				<p class="date_title"><?php the_time('Y年m月d日'); ?></p>
				<a class="trans art_title gb_link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a><!-- --><br><!--
                            --><small>カテゴリー:<?php foreach($cats as $cat): ?><a href="#"><?php echo $cat->name; ?></a><?php endforeach; ?>
					—  <?php the_author(); ?> @ <?php echo (get_the_time('A') == 'AM') ? '午前': '午後';  ?> <?php the_time('g:i'); ?></small>
				<br><br>
				<p class="news_content"><?php the_excerpt(); ?></p>
				<div class="fblike_and_comment">
					<div class="fb-like" data-href="#" data-layout="button_count" data-action="like" data-show-faces="true"></div>
					<a class="trans comment" href="<?php the_permalink() ?>">コメント <?php comments_number('(0)','(1)','(%)'); ?></a>
				</div>
			</div>
		<?php endwhile; endif; ?>
	</div>
</div><!-- / .main_post -->
<?php get_footer(); ?>